const url = 'https://rickandmortyapi.com/api/character';

const parentBlock = document.createElement("div");
parentBlock.classList.add("cardContainer");

fetch(url)
    .then(res => res.json())
    .then(data => {
        const rawData = data.results;
        return rawData.map(character => {
            //all needed data is listed below as an entity
            let created = character.created;
            let species = character.species ;
            let img = character.image;
            let name = character.name;
            let location = character.location;

            const cardElement = document.createElement("div");
            const text = document.createTextNode(`${created} ${name} ${location.name} ${species}`);
            const imgElement = document.createElement("img");
            imgElement.setAttribute("src", img);

            cardElement.append(text, imgElement);
            cardElement.classList.add("card");
            parentBlock.append(cardElement);
        });
    })
    .catch((error) => {
        console.log(JSON.stringify(error));
    });

document.addEventListener("DOMContentLoaded", () => {
    document.body.appendChild(parentBlock);
})